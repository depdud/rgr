import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PIL import Image
flash1 = 1
flash2 = 0
flash3 = 0
oz = 20
ox = 0
oy = 0
def loadTexture(fileName):
    image = Image.open(fileName)
    width = image.size[0]
    height = image.size[1]
    image = image.tobytes("raw", "RGBX", 0, -1)
    texture = glGenTextures(1)

    glBindTexture(GL_TEXTURE_2D, texture)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image)

    return texture

def init():
    global textureMetall
    global textureWood
    global textureCloth
    global textureWater

    textureWood = loadTexture("texture//1.jpg")
    textureMetall = loadTexture("texture//metall.jpg")
    textureCloth = loadTexture("texture//cloth.jpg")
    textureWater = loadTexture("texture//water.jpg")
    glClearColor(0.3, 0.3, 0.3, 1.0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_TEXTURE_2D)

    glEnable(GL_LIGHTING)  # Включаем освещение
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, (0.2, 0.2, 0.2, 0.2))
    glEnable(GL_NORMALIZE)

def reshape(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(75.0, float(width) / float(height), 1.0, 75.0)
    glutPostRedisplay()


def direct_light():
    glEnable(GL_LIGHT0)
    glLightfv(GL_LIGHT0, GL_AMBIENT, (1.0, 1.0, 1.0, 1.0))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, (1.0, 1.0, 1.0, 1.0))
    glLightfv(GL_LIGHT0, GL_POSITION, (0, 10, 20, 0))


def draw_spot_light():
    light4_diffuse = (0.4, 0.7, 0.2, 1)
    light4_position = (0, 3, 4, 1)
    glEnable(GL_LIGHT1)
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light4_diffuse)
    glLightfv(GL_LIGHT1, GL_POSITION, light4_position)
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, (0, 0, -1))
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF,  50)



def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(ox, oy, oz, 0, 0, 0, 0, 1, 0)

    glPushMatrix()
    Dno()
    sail()
    pillar()
    water()
    glPopMatrix()

    if flash1 == 1:
        direct_light()
    else:
        glDisable(GL_LIGHT0)

    if flash2 == 1:
        draw_spot_light()
    else:
        glDisable(GL_LIGHT1)
    if flash3 == 1:
        glDisable(GL_TEXTURE_2D)
    else:
        glEnable(GL_TEXTURE_2D)


    glutSwapBuffers()  # Выводим все нарисованное в памяти на экран


def specialkeys(key, x, y):
    global xrot
    global yrot
    global zrot
    global flash1
    global flash2
    global flash3

    if key == GLUT_KEY_F1:
        if flash1 == 0:
            flash1 = 1
        else:
            flash1 = 0
    if key == GLUT_KEY_F2:
        if flash2 == 0:
            flash2 = 1
        else:
            flash2 = 0
    if key == GLUT_KEY_F3:
        if flash3 == 0:
            flash3 = 1
        else:
            flash3 = 0
    if key == GLUT_KEY_PAGE_UP:
        zrot += 2.0
    if key == GLUT_KEY_PAGE_DOWN:
        zrot -= 2.0
    if key == GLUT_KEY_UP:
        xrot = min(xrot + 2.0,180)
    if key == GLUT_KEY_DOWN:
        xrot = max(xrot - 2.0, 0)
    if key == GLUT_KEY_LEFT:
        yrot -= 2.0
    if key == GLUT_KEY_RIGHT:
        yrot += 2.0

    glutPostRedisplay()  # Вызываем процедуру перерисовки

def keyboard(key, x, y):
    global oz
    global ox
    global oy

    if key == b'w':
        oz -= 1
    if key == b's':
        oz += 1
    if key == b'a':
        ox -= 1
    if key == b'd':
        ox += 1
    if key == b'e':
        oy -= 1
    if key == b'q':
        oy += 1
    glutPostRedisplay()

verticies = (
    #нижняя часть дна
    (4, 0, 3),#0
    (4, 0, -3),#1
    (-4, 0, -3),#2
    (-4, 0, 3),#3
     #верхняя часть дна
    (4, 2, 3),#4
    (4, 2, -3),#5
    (-4, 2, -3),#6
    (-4, 2, 3),#7
    #верхние края дна
    (7, 2, 0),  #8
    (-7, 2, 0),  #9

#низ столба
    (0.5, 2, 0.5),  #10
    (0.5, 2,-0.5),  #11
    (-0.5, 2, -0.5),  #12
    (-0.5, 2,0.5),  #13
#верх столба
    (0.5, 9, 0.5),  #14
    (0.5, 9, -0.5),   #15
    (-0.5, 9, -0.5),  #16
    (-0.5, 9, 0.5),   #17

#Парус
    (0.5, 3, 0),  #18
    (0.5, 9, 0),  #19
    (7,3,0),  #20
    )

def Dno():
    glBindTexture(GL_TEXTURE_2D, textureWood)
    # нижняя часть дна
    glBegin(GL_POLYGON)
    glNormal3f(0, -1, 0)
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[0])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[1])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[2])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[3])
    glEnd()
    # верхняя часть дна
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 0)
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[4])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[5])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[6])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[7])
    glEnd()
    #нижний край дна
    glBegin(GL_POLYGON)
    glNormal3f(1, 0, 0)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[0])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[1])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[8])
    glEnd()
    # нижний край дна(2)
    glBegin(GL_POLYGON)
    glNormal3f(-1, 0, 0)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[2])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[3])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[9])
    glEnd()
    # верхний стреугольник право
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 0)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[4])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[5])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[8])
    glEnd()
    # верхний стреугольник право
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 0)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[6])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[7])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[9])
    glEnd()
    # правый-верх бок треугольник
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 1)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[0])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[4])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[8])
    glEnd()
 # левы -верх бок треугольник
    glBegin(GL_POLYGON)
    glNormal3f(0, -1, -1)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[1])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[5])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[8])
    glEnd()
    # правый-низ бок треугольник
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 1)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[3])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[7])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[9])
    glEnd()
    # левы -низ бок треугольник
    glBegin(GL_POLYGON)
    glNormal3f(0, -1, -1)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[2])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[6])
    glTexCoord2f(0.5, 1)
    glVertex3fv(verticies[9])
    glEnd()
# правый бок
    glBegin(GL_POLYGON)
    glNormal3f(0, 0, 1)
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[0])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[4])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[7])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[3])
    glEnd()
    # левый бок
    glBegin(GL_POLYGON)
    glNormal3f(0, 0, -1)
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[1])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[5])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[6])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[2])
    glEnd()
    #Столб
def pillar():
    glBindTexture(GL_TEXTURE_2D, textureMetall)
    # право
    glBegin(GL_POLYGON)
    glNormal3f(0, 0, 1)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[10])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[14])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[17])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[13])

    glEnd()
    # лево
    glBegin(GL_POLYGON)
    glNormal3f(0, 0, -1)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[11])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[15])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[16])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[12])
    glEnd()
    #up
    glBegin(GL_POLYGON)
    glNormal3f(1, 0, 0)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[10])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[14])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[15])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[11])
    glEnd()
    # down
    glBegin(GL_POLYGON)
    glNormal3f(-1, 0, 0)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[13])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[17])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[16])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[12])
    glEnd()
    # top
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 0)
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[14])
    glTexCoord2f(1, 1)
    glVertex3fv(verticies[15])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[16])
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[17])
    glEnd()

#Парус
def sail():
    glBindTexture(GL_TEXTURE_2D, textureCloth)
    glBegin(GL_POLYGON)
    glNormal3f(0, 0, -1)
    glTexCoord2f(0, 0)
    glVertex3fv(verticies[18])
    glTexCoord2f(0, 1)
    glVertex3fv(verticies[19])
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[20])
    glEnd()

    glBegin(GL_POLYGON)
    glNormal3f(0, 0, 1)
    glTexCoord2f(0, 0)
    glVertex3fv((0.5, 3, 0.1 ))
    glTexCoord2f(0, 1)
    glVertex3fv((0.5, 9, 0.1 ))
    glTexCoord2f(1, 0)
    glVertex3fv(verticies[20])
    glEnd()
#Море
def water():
    glBindTexture(GL_TEXTURE_2D,textureWater)
    glBegin(GL_POLYGON)
    glNormal3f(0, 1, 0)
    glTexCoord2f(1, 1)
    glVertex3fv((10, 0, 10))
    glTexCoord2f(0, 1)
    glVertex3fv((10, 0, -10))
    glTexCoord2f(0, 0)
    glVertex3fv((-10, 0, -10))
    glTexCoord2f(1, 0)
    glVertex3fv((-10, 0, 10))
    glEnd()
# Здесь начинается выполнение программы
# Использовать двойную буферизацию и цвета в формате RGB (Красный, Зеленый, Синий)
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
# Указываем начальный размер окна (ширина, высота)
glutInitWindowSize(800, 600)
# Указываем начальное положение окна относительно левого верхнего угла экрана
glutInitWindowPosition(50, 50)
# Инициализация OpenGl
glutInit(sys.argv)
# Создаем окно с заголовком "Happy New Year!"
glutCreateWindow(b"My prorgramm")
# Определяем процедуру, отвечающую за перерисовку
glutDisplayFunc(display)
glutReshapeFunc(reshape)
# Определяем процедуру, отвечающую за обработку клавиш
glutSpecialFunc(specialkeys)
glutKeyboardFunc(keyboard)
init()
# Запускаем основной цикл
glutMainLoop()
